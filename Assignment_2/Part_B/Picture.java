package Part_B;

import Part_A.*;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Picture class contains many kids of Shapes which makes a Picture.
 * @author Gaurav Patel
 */
public class Picture extends Thread {
    private List<Shape> sources;
    private double perimeter;
    private double area;
    
    /**
     * Default constructor set's all parameters value to zero and creates a ArrayList().
     */
    public Picture(){
        sources = new ArrayList();
        perimeter = 0.00;
        area = 0.00;
    }
    
    /**
     * This method helps retrieve specific object from List of Shapes.
     * @param i index of object to retrieve.
     * @return Shape object at index i 
     */
    public Object getShape(int i){
        if(i <= sources.size()){
            return sources.get(i);
        }else{
            System.err.println("No object exist at passed in index.");
        }
        
        return null;
    }
    
    /**
     * This method adds Shape object to List of shapes.
     * @param obj The object is added to List of Shapes.
     */
    public void addShape(Shape obj){
        sources.add(obj);
    }
    
    @Override
    public void run(){
        System.out.println("Checking run()");
        perimeter += this.getPerimeter();
        area += this.getArea();
    }
    
    /**
     * This method calculates perimeter and area of the Picture.
     */
    public void calculate(){
        perimeter = 0.00;
        area = 0.00;
        
        Thread[] t = new Thread[sources.size()];
		
        for(int i=0; i < sources.size(); i++){
            final int index = i;
            t[i] = new Thread(){ 
                @Override
            	public void run(){
            		perimeter += (sources.get(index)).getPerimeter();
            		area += (sources.get(index)).getArea();
            	}
            };
            t[i].start();
            
            for(int c=0; c < sources.size(); c++){
                try {
                    t[i].join();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }//End of calculate() function.
    
    /**
     * This method allows user to retrieve latest Perimeter of Picture by calling calculate function.
     * @return Perimeter of the current object.
     */
    public double getPerimeter(){
        calculate();
        return perimeter;
    }
    
    /**
     * This method allows user to retrieve latest Area of Picture by calling calculate function.
     * @return Area of the current object.
     */
    public double getArea(){
        calculate();
        return area;
    }
    /**
     * This method generates String form of the object.
     * @return Object's data as name and values pair as a string.
     */
    @Override
    public String toString(){
        String ret_value = "Picture contains following Shapes: \n";
        
        for(Shape obj : sources){
            ret_value += obj.toString();
        }
        
        return ret_value;
    }
    
    /**
     * This method checks if the current object is equals to passed in object.
     * @param obj Object to compare the current object.
     * @return True if test passes, False otherwise.
     */
    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Picture other = (Picture) obj;
        if (this.sources != other.sources && (this.sources == null || !this.sources.equals(other.sources))) {
            return false;
        }
        if (Double.doubleToLongBits(this.perimeter) != Double.doubleToLongBits(other.perimeter)) {
            return false;
        }
        if (Double.doubleToLongBits(this.area) != Double.doubleToLongBits(other.area)) {
            return false;
        }
        return true;
    }
    
    /**
     * This method generates unique object identifier to check against Object method. 
     * @return Unique identifier of specific object.
     */
    @Override
    public int hashCode() {
        int hash = 7;
        hash = 67 * hash + (this.sources != null ? this.sources.hashCode() : 0);
        hash = 67 * hash + (int) (Double.doubleToLongBits(this.perimeter) ^ (Double.doubleToLongBits(this.perimeter) >>> 32));
        hash = 67 * hash + (int) (Double.doubleToLongBits(this.area) ^ (Double.doubleToLongBits(this.area) >>> 32));
        return hash;
    }
    
    /**
     * This method makes another copy of current object and returns it.
     * @return Copy of current object.
     * @throws CloneNotSupportedException 
     */
    @Override
    public Object clone() throws CloneNotSupportedException{
        super.clone();
        
        Picture src = new Picture();
        
        for(int i=0; i < sources.size(); i++){
            src.addShape(sources.get(i));
        }
        
        return src;
    }
    
    /**
     * This method reads file data and creates Picture class according to data from file.
     * @param file File name to read data off.
     * @return True if successful, False otherwise.
     */
    public boolean readFile(String file){
        int circle_counter = 0;
        int square_counter = 0;
        int rectangle_counter = 0;
        int parallelogram_counter = 0;
        int triangle_counter = 0;
           
        BufferedReader in = null;
        try{ 
        //Reading the file and checking if it meets Picture object requirements. each shape with 2 minimum objects.
            in = new BufferedReader(new FileReader(file));
            String data;
        
            while((data = in.readLine()) != null){
                String [] values = data.split(",");
                    
                if(values[0].toUpperCase().equals("CIRCLE")){
                    circle_counter++;
                        
                }else if(values[0].toUpperCase().equals("TRIANGLE")){
                    triangle_counter++;
                    
                }else if(values[0].toUpperCase().equals("RECTANGLE")){
                    rectangle_counter++;
                    
                }else if(values[0].toUpperCase().equals("SQUARE")){
                    square_counter++;
                    
                }else if(values[0].toUpperCase().equals("PARALLELOGRAM")){
                    parallelogram_counter++;
                }
            }
            in.close();
                
        }catch(IOException e){
            e.getMessage();
            e.printStackTrace();
            return false;
        }
            
        if(circle_counter >= 2 && triangle_counter >= 2 && rectangle_counter >= 2
                    && square_counter >= 2 && parallelogram_counter >= 2){
            //Creating Shapes and adding it to list in Picture.
        
            try{
                in = new BufferedReader(new FileReader(file));
                String data;
            
                while((data = in.readLine()) != null){
                    String [] values = data.split(",");

                    if(values[0].toUpperCase().equals("CIRCLE")){
                        Double r = Double.parseDouble(values[1]);
                        sources.add(new Circle(r));
                            
                    }else if(values[0].toUpperCase().equals("TRIANGLE")){
                        Double a = Double.parseDouble(values[1]);
                        Double b = Double.parseDouble(values[2]);
                        Double c = Double.parseDouble(values[3]);
                            
                        sources.add(new Triangle(a,b,c));
                           
                    }else if(values[0].toUpperCase().equals("RECTANGLE")){
                        Double l = Double.parseDouble(values[1]);
                        Double w = Double.parseDouble(values[2]);
                            
                        sources.add(new Rectangle(l,w));
                            
                    }else if(values[0].toUpperCase().equals("SQUARE")){
                        Double s = Double.parseDouble(values[1]);
                            
                        sources.add(new Square(s));
                            
                    }else if(values[0].toUpperCase().equals("PARALLELOGRAM")){
                        Double l = Double.parseDouble(values[1]);
                        Double w = Double.parseDouble(values[2]);
                        Double h = Double.parseDouble(values[3]);
                            
                        sources.add(new Parallelogram(l,w,h));
                    }
                }
                in.close();
                
                return true;
                
            }catch(IOException e){    
                e.getMessage();
                e.printStackTrace();
                return false;
            }
                
        }else{
            System.out.println("Not enough number of shapes are passed in. There must be 2 shape of each type.");
            
            return false;
        }
    } //End of readFile(); function
    
}
