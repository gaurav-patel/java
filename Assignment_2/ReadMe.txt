Java444 - Assignment 2
-----------------------
-> Total: 3 Folders, 8 ".java", and 1 ".txt" files


Folder-> Part_A:
	--> Shape.java: Shape interface that contains two methods that other type of shape 
		classes(ex: Circle, Square) must contain.

	--> Circle.java: Purpose is to hold information(radius) of circle shape and calculate
		and return information on Perimeter and area.

	--> Parallelogram.java: Purpose is to hold information(length, width, height) of 
		Parallelogram shape and to calculate and return information on perimeter and area.

	--> Rectangle.java: Purpose is to hold information(length and width) of Rectangle
		shape and to calculate and return information on perimeter and area.
	
	--> Square.java: Purpose is to hold information(length of all sides) of Square shape 
		and to calculate and return information on perimeter and area.
	
	--> Triangle.java: Purpose is to hold information(length of the three sides) of Triangle
		shape and to calculate and return information on perimeter and area.

Folder-> Part_B:
	--> Picture.java: Purpose is to hold multiple types of Shapes and to calculate perimeter
		and area of all shapes. Also, read from text file and create objects using file 
		information and add it into the Picture class list of shapes and add user created
		shape object as well.

Folder-> test:
	--> Assignement_2_Test.java: The purpose is to test all the classes and make sure they work
		properly and provide result required by each class function. Note:(Created in netbeans
		and had to add Junit4.10.jar file to built library.)
Folder-> Assignment_2 (Project Folder):
	--> Test.txt: The file contains all data to run "Assignment_2_Test.java" file. It contains
		names and parameter required to create Shapes. "Assignment_2_Test.java" file tests
		Picture class using this file.