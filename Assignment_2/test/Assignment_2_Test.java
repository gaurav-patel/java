/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import org.junit.Test;
import static org.junit.Assert.*;

import Part_A.*;
import Part_B.Picture;

/**
 *
 * @author Ambica
 */
public class Assignment_2_Test {
    @Test
    public void testCircle(){
        Circle c = new Circle(5);
        
        assertEquals("Testing for Perimeter of Circle(5): ", 31.42, c.getPerimeter(), 0.01);
        assertEquals("Testing for Area of Circle(5): ", 78.54, c.getArea(), 0.01);
      
        c = new Circle(-5.4);
        assertEquals("Testing for Perimeter of Circle(-5.4): ", 0.00, c.getPerimeter(), 0.01);
        assertEquals("Testing for Area of Circle(-5.4): ", 0.01, c.getArea(), 0.01); 
        
        c = new Circle(2.14);
        assertEquals("Testing for Perimeter of Circle(2.14): ", 13.45, c.getPerimeter(), 0.01);
        assertEquals("Testing for Area of Circle(2.14): ", 14.39, c.getArea(), 0.01); 
    }
    
    @Test
    public void testSquare(){
        Square s = new Square(5.243);
        assertEquals("Testing for Perimeter of Square(5.243): ", 20.97, s.getPerimeter(), 0.01);
        assertEquals("Testing for Area of Square(5.243): ", 27.49, s.getArea(), 0.01);
        
        s = new Square(-2.2);
        assertEquals("Testing for Perimeter of Square(-2.2): ", 0.00, s.getPerimeter(), 0.01);
        assertEquals("Testing for Area of Square(-2.2): ", 0.00, s.getArea(), 0.01);
        
        s = new Square(0.123);
        assertEquals("Testing for Perimeter of Square(0.123): ", 0.49, s.getPerimeter(), 0.01);
        assertEquals("Testing for Area of Square(0.123): ", 0.015, s.getArea(), 0.001);
    }
    
    @Test
    public void testRectangle(){
        Rectangle s = new Rectangle(5.24, 3.1);
        assertEquals("Testing for Perimeter of Square(5.24, 3.1): ", 16.68, s.getPerimeter(), 0.01);
        assertEquals("Testing for Area of Square(5.24, 3.1): ", 16.24, s.getArea(), 0.01);
        
        s = new Rectangle(-10, 10);
        assertEquals("Testing for Perimeter of Square(-10, 10): ", 0.00, s.getPerimeter(), 0.01);
        assertEquals("Testing for Area of Square(-10, 10): ", 0.00, s.getArea(), 0.01);
        
        s = new Rectangle(0.123, 0.957);
        assertEquals("Testing for Perimeter of Square(0.123, 0.957): ", 2.16, s.getPerimeter(), 0.01);
        assertEquals("Testing for Area of Square(0.123, 0.957): ", 0.12, s.getArea(), 0.01);
    }
    
    @Test
    public void testParallelogram(){
        Parallelogram s = new Parallelogram(10.101, 1.05, 3);
        assertEquals("Testing for Perimeter of Parallelogram(10.101, 1.05, 3): ", 22.3, s.getPerimeter(), 0.01);
        assertEquals("Testing for Area of Parallelogram(10.101, 1.05, 3): ", 30.3, s.getArea(), 0.01);
        
        s = new Parallelogram(1.154, -2.01, 4);
        assertEquals("Testing for Perimeter of Parallelogram(1.154, -2.01, 4): ", 0.00, s.getPerimeter(), 0.01);
        assertEquals("Testing for Area of Parallelogram(1.154, -2.01, 4): ", 0.00, s.getArea(), 0.01);
        
        s = new Parallelogram(0.14, 2.1101, -1);
        assertEquals("Testing for Perimeter of Parallelogram(0.14, 2.1101, -1): ", 0.00, s.getPerimeter(), 0.01);
        assertEquals("Testing for Area of Parallelogram(0.14, 2.1101, -1): ", 0.00, s.getArea(), 0.01);
        
        s = new Parallelogram(0.2151, 1.24, 8);
        assertEquals("Testing for Perimeter of Parallelogram(0.215, 1.24, 8): ", 2.91, s.getPerimeter(), 0.01);
        assertEquals("Testing for Area of Parallelogram(0.215, 1.24, 8): ", 1.72, s.getArea(), 0.01);
    }
    
    @Test
    public void testTriangle(){
        Triangle s = new Triangle(10.123, -1.234, 0.9785);
        assertEquals("Testing for Perimeter of Triangle(0.123, 1.234, 0.9785): ", 0.00, s.getPerimeter(), 0.00);
        assertEquals("Testing for Area of Triangle(0.123, 1.234, 0.9785): ", 0.00, s.getArea(), 0.00);
        
        s = new Triangle(1.1, 10.2, 0.-85);
        assertEquals("Testing for Perimeter of Triangle(1.1, 10.2, 0.-85): ", 0.00, s.getPerimeter(), 0.00);
        assertEquals("Testing for Area of Triangle(1.1, 10.2, 0.-85): ", 0.00, s.getArea(), 0.01);
        
        s = new Triangle(0.123, 1.034, 0.9785);
        assertEquals("Testing for Perimeter of Triangle(0.123, 1.034, 0.978): ", 2.1355, s.getPerimeter(), 0.001);
        assertEquals("Testing for Area of Triangle(0.123, 1.034, 0.978): ", 0.05512, s.getArea(), 0.0001);
    }
    
    @Test
    public void testPicture(){
        Picture s = new Picture();
        s.readFile("Test.txt");
        
        Shape[] temp = {new Triangle(3,6,7), new Circle(101), new Rectangle(5,8), new Triangle(6,8,12),
            new Parallelogram(10,5,3), new Parallelogram(8,3,1), new Rectangle(4,2), new Square(5), 
            new Circle(50), new Square(10)};
        
        double expected_perimeter = 0.00;
        double expected_area = 0.00;
        
        for(int i=0; i < temp.length; i++){
            expected_perimeter += temp[i].getPerimeter();
            expected_area += temp[i].getArea();
        }
        
        assertEquals("Testing for Perimeter of Picture().readFile('Test.txt'): ", expected_perimeter, s.getPerimeter(), 0.001);
        assertEquals("Testing for Area of Picture().readFile('Test.txt'): ", expected_area, s.getArea(), 0.001);
    }
}
