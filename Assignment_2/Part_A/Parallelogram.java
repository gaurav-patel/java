package Part_A;

/**
 * Parallelogram class contains length of two sides and height of Parallelogram and implements Shape interface.
 * @author Gaurav
 */
public class Parallelogram implements Shape {
    private double length;
    private double width;
    private double height;
    
    /**
     * Default constructor sets private field values to zero. 
     */
    public Parallelogram(){
        length = 0;
        width = 0;
        height = 0;
    }
    
    /**
     * Overload Constructor sets value of passed in length, width and height to current object's value if value is greater then zero.
     * @param l Length to set for current object.
     * @param w Width to set for current object.
     * @param h Height to set for current object.
     */
    public Parallelogram(double l, double w, double h){
        if(l > 0 && w > 0 && h > 0){
            length = l;
            width = w;
            height = h;
        }else{
            System.err.println("Length and Width and Height of the Parallelogram shape must be positive.");
        }
    }
    
    /**
     * This method allows user to set new Length of the Parallelogram object.
     * @param l Length to set for current object.
     * @return True if successful, False otherwise.
     */
    public boolean setLength(double l){
        if(l > 0){
            length = l;
            return true;
        }else{
            System.err.println("Length of the Parallelogram must be positive.");
            return false;
        }
    }
    
    /**
     * This method allows user to set new Width of the Parallelogram object.
     * @param w Width to set for current object.
     * @return True if successful, False otherwise.
     */
    public boolean setWidth(double w){
        if(w > 0){
            width = w;
            return true;
        }else{
            System.err.println("Width of the Parallelogram must be positive.");
            return false;
        }
    }
    
    /**
     * This method allows user to set new Height of the Parallelogram object.
     * @param h Height to set for Parallelogram object.
     * @return True if successful, False otherwise.
     */
    public boolean setHeight(double h){
        if(h > 0){
            height = h;
            return true;
        }else{
            System.err.println("Height of the Parallelogram must be positive.");
            return false;
        }
    }
    
    /**
     * This method allows user to retrieve Length of Parallelogram object.
     * @return Length of the current object.
     */
    public double getLength(){
        return length;
    }
    
    /**
     * This method allows user to retrieve Width of Parallelogram object.
     * @return Width of the current object.
     */
    public double getWidth(){
        return width;
    }
    
    /**
     * This method allows user to retrieve Height of Parallelogram object.
     * @return Height of the current object.
     */
    public double getHeight(){
        return height;
    }
    
    /**
     * This method calculates perimeter of the Parallelogram object.
     * @return Perimeter of the current object.
     */
    @Override
    public double getPerimeter() {
        return (2 * length) + (2 * width);
    }

    /**
     * This method calculates area of Parallelogram object.
     * @return Area of current object.
     */
    @Override
    public double getArea() {
        return length * height;
    }
    
    /**
     * This method generates unique object identifier to check against Object method. 
     * @return Unique identifier of specific object.
     */
    @Override
    public int hashCode() {
        int hash = 7;
        hash = 67 * hash + (int) (Double.doubleToLongBits(this.length) ^ (Double.doubleToLongBits(this.length) >>> 32));
        hash = 67 * hash + (int) (Double.doubleToLongBits(this.width) ^ (Double.doubleToLongBits(this.width) >>> 32));
        hash = 67 * hash + (int) (Double.doubleToLongBits(this.height) ^ (Double.doubleToLongBits(this.height) >>> 32));
       
        return hash;
    }

    /**
     * This method checks if the current object is equals to passed in object.
     * @param obj Object to compare the current object.
     * @return True if test passes, False otherwise.
     */
    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Parallelogram other = (Parallelogram) obj;
        if (Double.doubleToLongBits(this.length) != Double.doubleToLongBits(other.length)) {
            return false;
        }
        if (Double.doubleToLongBits(this.width) != Double.doubleToLongBits(other.width)) {
            return false;
        }
        if (Double.doubleToLongBits(this.height) != Double.doubleToLongBits(other.height)) {
            return false;
        }
        
        return true;
    }

    /**
     * This method generates String form of the object.
     * @return Object's data as name and values pair as a string.
     */
    @Override
    public String toString() {
        return "Parallelogram {" + "Length=" + length + ", Width=" + width + ", Height=" + height + "}\n";
    }
    
    /**
     * This method makes another copy of current object and returns it.
     * @return Copy of current object.
     * @throws CloneNotSupportedException 
     */
    @Override
    protected Object clone() throws CloneNotSupportedException {
        super.clone(); 
        Parallelogram src = new Parallelogram(length, width, height);
        
        return src;
    }
    
}
