package Part_A;

/**
 * Triangle class contains length of three sides of Triangle and implements Shape interface. 
 * @author Gaurav
 */
public class Triangle implements Shape {
    private double side_a;
    private double side_b;
    private double side_c;
    private double height;
    
    /**
     * Default constructor sets private field values to zero. 
     */
    public Triangle(){
        side_a = 0;
        side_b = 0;
        side_c = 0;
        height = 0;
    }
    
    /**
     * Overload Constructor sets value of passed in SideA, SideB and SideC to current object's value if value is greater then zero.
     * @param a SideA to set for current object.
     * @param b SideB to set for current object.
     * @param c SideC to set for current object.
     */
    public Triangle(double a, double b, double c){
        double semi_para = (a + b + c) / 2;
        
        if(a > 0 && a < semi_para && b > 0 && b < semi_para && c > 0 && c < semi_para){
            side_a = a;
            side_b = b;
            side_c = c;
            height = semi_para;
        }else{
            System.err.println("All sides of the Triangle shape must be positive and all sides must be less then total sides divided by 2.");
        }
    }
    
    /**
     * This method allows user to set new Side A of the Triangle object.
     * @param a SideA to set for current object.
     * @return True if successful, False otherwise.
     */
    public boolean setSideA(double a){
        if(a > 0 && a < height){
            side_a = a;
            return true;
        }else{
            System.err.println("All sides of the Triangle shape must be positive and all sides must be less then total sides divided by 2.");
            return false;
        }
    }
    
    /**
     * This method allows user to set new Side B of the Triangle object.
     * @param b SideB to set for current object.
     * @return True if successful, False otherwise.
     */
    public boolean setSideB(double b){
        if(b > 0 && b < height){
            side_b = b;
            return true;
        }else{
            System.err.println("All sides of the Triangle shape must be positive and all sides must be less then total sides divided by 2.");
            return false;
        }
    }
    
    /**
     * This method allows user to set new Side C of the Triangle object.
     * @param c SideC to set for current object.
     * @return True if successful, False otherwise.
     */
    public boolean setSideC(double c){
        if(c > 0 && c < height){
            side_c = c;
            return true;
        }else{
            System.err.println("All sides of the Triangle shape must be positive and all sides must be less then total sides divided by 2.");
            return false;
        }
    }
    
    /**
     * This method allows user to retrieve Side A of Triangle object.
     * @return Side A of the current object.
     */
    public double getSideA(){
        return side_a;
    }
    
    /**
     * This method allows user to retrieve Side B of Triangle object.
     * @return Side B of the current object.
     */
    public double getSideB(){
        return side_b;
    }
    
    /**
     * This method allows user to retrieve Side C of Triangle object.
     * @return Side C of the current object.
     */
    public double getSideC(){
        return side_c;
    }
    /**
     * This method calculates Perimeter of the current object.
     * @return Perimeter of the current object.
     */
    @Override
    public double getPerimeter() {
        return side_a + side_b + side_c;
    }
    
    /**
     * This method calculates Area of Circle object.
     * @return Area of current object.
     */
    @Override
    public double getArea() {
        
        double area = height * (height - side_a) * (height - side_b) * (height - side_c);
        area = Math.sqrt(area);
        
        return area;
    }

    /**
     * This method generates unique object identifier to check against Object method. 
     * @return Unique identifier of specific object.
     */
    @Override
    public int hashCode() {
        int hash = 3;
        hash = 89 * hash + (int) (Double.doubleToLongBits(this.side_a) ^ (Double.doubleToLongBits(this.side_a) >>> 32));
        hash = 89 * hash + (int) (Double.doubleToLongBits(this.side_b) ^ (Double.doubleToLongBits(this.side_b) >>> 32));
        hash = 89 * hash + (int) (Double.doubleToLongBits(this.side_c) ^ (Double.doubleToLongBits(this.side_c) >>> 32));
        
        return hash;
    }
    
    /**
     * This method checks if the current object is equals to passed in object.
     * @param obj Object to compare the current object.
     * @return True if test passes, False otherwise.
     */
    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Triangle other = (Triangle) obj;
        if (Double.doubleToLongBits(this.side_a) != Double.doubleToLongBits(other.side_a)) {
            return false;
        }
        if (Double.doubleToLongBits(this.side_b) != Double.doubleToLongBits(other.side_b)) {
            return false;
        }
        if (Double.doubleToLongBits(this.side_c) != Double.doubleToLongBits(other.side_c)) {
            return false;
        }
        
        return true;
    }
    
    /**
     * This method generates String form of the object.
     * @return Object's data as name and values pair as a string.
     */
    @Override
    public String toString() {
        return "Triangle{" + "Side A=" + side_a + ", Side B=" + side_b + ", Side C=" + side_c + "}\n";
    }
    
    /**
     * This method makes another copy of current object and returns it.
     * @return Copy of current object.
     * @throws CloneNotSupportedException 
     */
    @Override
    protected Object clone() throws CloneNotSupportedException {
        super.clone(); 
        Triangle src = new Triangle(side_a, side_b, side_c);
        
        return src;
    }
    
}
