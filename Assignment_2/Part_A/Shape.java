package Part_A;

/**
 * This interface forces implemented classes to have following methods.
 * @author Gaurav
 */
public interface Shape {
    public double getPerimeter();
    public double getArea();
}
