package Part_A;

/**
 * Circle class contains length of radius and implements Shape interface.
 * @author Gaurav
 */
public class Circle implements Shape {
    private double radius;
    
    /**
     * Default constructor sets private field values to zero. 
     */
    public Circle(){
        radius = 0;
    }
    
    /**
     * Overload Constructor sets value of passed in Radius to current object's value if value is greater then zero.
     * @param r Radius to set for current object.
     */
    public Circle(double r){
        if(r > 0){
            radius = r;
        }else{
            System.err.println("Radius of the Circle must be positive.");
        }
    }
    
    /**
     * This method allows user to set new Radius of the Circle object.
     * @param r Radius to set for current object.
     * @return True if successful, False otherwise.
     */
    public boolean setRadius(double r){
        if(r > 0){
            radius = r;
            return true;
        }else{
            System.err.println("Radius of the Circle must be positive.");
            return false;
        }
    }
    
    /**
     * This method allows user to retrieve Radius of Circle object.
     * @return Radius of the current object.
     */
    public double getRadius(){
        return radius;
    }
    
    /**
     * This method calculates Perimeter of the current object.
     * @return Perimeter of the current object.
     */
    @Override
    public double getPerimeter() {
        return 2 * Math.PI * radius;
    }
    
    /**
     * This method calculates Area of Circle object.
     * @return Area of current object.
     */
    @Override
    public double getArea() {
        return Math.PI * (radius * radius);
    }
    
    /**
     * This method generates unique object identifier to check against Object method. 
     * @return Unique identifier of specific object.
     */
    @Override
    public int hashCode() {
        int hash = 7;
        hash = 19 * hash + (int) (Double.doubleToLongBits(this.radius) ^ (Double.doubleToLongBits(this.radius) >>> 32));
        
        return hash;
    }
    
    /**
     * This method checks if the current object is equals to passed in object.
     * @param obj Object to compare the current object.
     * @return True if test passes, False otherwise.
     */
    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Circle other = (Circle) obj;
        if (Double.doubleToLongBits(this.radius) != Double.doubleToLongBits(other.radius)) {
            return false;
        }
        
        return true;
    }

    /**
     * This method generates String form of the object.
     * @return Object's data as name and values pair as a string.
     */
    @Override
    public String toString() {
        return "Circle {" + "Radius=" + radius + "}\n";
    }
    
    /**
     * This method makes another copy of current object and returns it.
     * @return Copy of current object.
     * @throws CloneNotSupportedException 
     */
    @Override
    protected Object clone() throws CloneNotSupportedException {
        super.clone(); 
        Circle src = new Circle(radius);
        
        return src;
    }

}
