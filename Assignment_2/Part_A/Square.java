package Part_A;

/**
 * Square class contains Length of side and implements Shape interface.
 * @author Gaurav
 */
public class Square implements Shape {
    private double length;
    
    /**
     * Default constructor sets private field values to zero. 
     */
    public Square(){
        length = 0;
    }
    
    /**
     * Overload Constructor sets value of passed in Length to current object's value if value is greater then zero.
     * @param l Length to set for Square object.
     */
    public Square(double l){
        if(l > 0){
            length = l;
        }else{
            System.err.println("Length of the Square shape must be positive.");
        }
    }
    
    /**
     * This method allows user to set new Length of the Square object.
     * @param l Length to set for Square object.
     * @return True if successful, False otherwise.
     */
    public boolean setLength(double l){
        if(l > 0){
            length = l;
            return true;
        }else{
            System.err.println("Length of the Square shape must be positive.");
            return false;
        }
    }
    
    /**
     * This method allows user to retrieve Length of Square object.
     * @return Length of the current object.
     */
    public double getLength(){
        return length;
    }
    
    /**
     * This method calculates Perimeter of the Square object.
     * @return Perimeter of the current object.
     */
    @Override
    public double getPerimeter() {
        return length * 4;
    }
    
    /**
     * This method calculates Area of Square object.
     * @return Area of current object.
     */
    @Override
    public double getArea() {
        return length * length;
    }
    
    /**
     * This method generates unique object identifier to check against Object method. 
     * @return Unique identifier of specific object.
     */
    @Override
    public int hashCode() {
        int hash = 7;
        hash = 23 * hash + (int) (Double.doubleToLongBits(this.length) ^ (Double.doubleToLongBits(this.length) >>> 32));
        
        return hash;
    }

    /**
     * This method checks if the current object is equals to passed in object.
     * @param obj Object to compare the current object.
     * @return True if test passes, False otherwise.
     */
    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Square other = (Square) obj;
        if (Double.doubleToLongBits(this.length) != Double.doubleToLongBits(other.length)) {
            return false;
        }
        
        return true;
    }
    
    /**
     * This method generates String form of the object.
     * @return Object's data as name and values pair as a string.
     */
    @Override
    public String toString() {
        return "Square {" + "Length=" + length + "}\n";
    }

    /**
     * This method makes another copy of current object and returns it.
     * @return Copy of current object.
     * @throws CloneNotSupportedException 
     */
    @Override
    protected Object clone() throws CloneNotSupportedException {
        super.clone(); 
        Square src = new Square(length);
        
        return src;
    }
    
}
