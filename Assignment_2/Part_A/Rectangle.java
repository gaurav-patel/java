package Part_A;

/**
 * Rectangle class contains length of two sides of Rectangle and implements Shape interface.
 * @author Gaurav
 */
public class Rectangle implements Shape {
    private double length;
    private double width;
    
    /**
     * Default constructor sets private field values to zero. 
     */
    public Rectangle(){
        length = 0;
        width = 0;
    }
    
    /**
     * Overload Constructor sets value of passed in Length and Width to current object's value if value is greater then zero.
     * @param l Length to set for Rectangle object.
     * @param w Width to set for Rectangle object.
     */
    public Rectangle(double l, double w){
        if(l > 0 && w > 0){
            length = l;
            width = w;
        }else{
            System.err.println("Length and Width of the Rectangle shape must be positive.");
        }
    }
    
    /**
     * This method allows user to set new Length of the Rectangle object.
     * @param l Length to set for current object.
     * @return True if successful, False otherwise.
     */
    public boolean setLength(Double l){
        if(l > 0){
            length = l;
            return true;
        }else{
            System.err.println("Length and Width of the Rectangle shape must be positive.");
            return false;
        }
    }
    
    /**
     * This method allows user to set new Width of the Rectangle object.
     * @param w Width to set for current object.
     * @return True if successful, False otherwise.
     */
    public boolean setWidth(Double w){
        if(w > 0){
            width = w;
            return true;
        }else{
            System.err.println("Length and Width of the Rectangle shape must be positive.");
            return false;
        }
    }
    
    /**
     * This method allows user to retrieve Length of Rectangle object.
     * @return Length of the current object.
     */
    public double getLength(){
        return length;
    }
    
    /**
     * This method allows user to retrieve Width of Rectangle object.
     * @return Width of the current object.
     */
    public double getWidth(){
        return width;
    }
    
    /**
     * This method calculates Perimeter of the Rectangle object.
     * @return Perimeter of the current object.
     */
    @Override
    public double getPerimeter() {
        return (2 * length) + (2 * width);
    }
    
    /**
     * This method calculates Area of Rectangle object.
     * @return Area of current object.
     */
    @Override
    public double getArea() {
        return length * width;
    }
    
    /**
     * This method generates unique object identifier to check against Object method. 
     * @return Unique identifier of specific object.
     */
    @Override
    public int hashCode() {
        int hash = 5;
        hash = 53 * hash + (int) (Double.doubleToLongBits(this.length) ^ (Double.doubleToLongBits(this.length) >>> 32));
        hash = 53 * hash + (int) (Double.doubleToLongBits(this.width) ^ (Double.doubleToLongBits(this.width) >>> 32));
     
        return hash;
    }

    /**
     * This method checks if the current object is equals to passed in object.
     * @param obj Object to compare the current object.
     * @return True if test passes, False otherwise.
     */
    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Rectangle other = (Rectangle) obj;
        if (Double.doubleToLongBits(this.length) != Double.doubleToLongBits(other.length)) {
            return false;
        }
        if (Double.doubleToLongBits(this.width) != Double.doubleToLongBits(other.width)) {
            return false;
        }
        
        return true;
    }
    
    /**
     * This method generates String form of the object.
     * @return Object's data as name and values pair as a string.
     */
    @Override
    public String toString() {
        return "Rectangle {" + "Length=" + length + ", Width=" + width + "}\n";
    }
    
    /**
     * This method makes another copy of current object and returns it.
     * @return Copy of current object.
     * @throws CloneNotSupportedException 
     */
    @Override
    protected Object clone() throws CloneNotSupportedException {
        super.clone(); 
        Rectangle src = new Rectangle(length, width);
        
        return src;
    }
    
    
}
