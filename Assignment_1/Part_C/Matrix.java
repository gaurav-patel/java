package Part_C;
import java.util.Scanner;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *Creates Block for Matrix according to user input and finds the biggest block of 1's.
 * @author Gaurav
 */
public class Matrix{
    public static void main(String[] args){
        //Defining variables for later use.
        Scanner input = new Scanner(System.in);
        int Default = 5; //Default columns for Matrix.
	int User_rows;
        //Getting user input for number of rows and validating the value.
        System.out.print("Enter no of rows in the square matrix:");
        User_rows = input.nextInt();
		
	if(User_rows < 0){
		System.out.println("You have chosen incorrect amount of rows.");
		System.out.print("Please Enter no of rows greater then 0 for the square matrix:");
		User_rows = input.nextInt();
	}
		
        //Creating 2D array and getting values from the user.
        int [][] squareMatrix = new int[User_rows][Default];
        
        for(int r=0; r < User_rows; r++){
            for(int c=0; c < 5; c++){
                System.out.print("Enter the value for row " + (r+1) + " at Column " + (c+1) + ": ");
                squareMatrix[r][c] = input.nextInt();
            }
        }
        
        int minimum = 0;
        int maximum = 0;
        int row = 0;
        int column = 0;
        //marking the 2d array to see find largest block. 
        for(int r = 1; r < User_rows; r++){
            for(int c = 1; c < Default; c++){
                
                minimum = Math.min(squareMatrix[r][c-1], squareMatrix[r-1][c]);
                minimum = Math.min(minimum, squareMatrix[r-1][c-1]);

                if(squareMatrix[r][c] == 1){
                    squareMatrix[r][c] = minimum + 1;
                }
                else{
                    squareMatrix[r][c] = 0;
                }
            }
        }
        //Looking for biigest number which was marked in above for loop.
        for(int r = 0; r < User_rows; r++){
            for(int c = 0; c < Default; c++){
                if(maximum < squareMatrix[r][c]){
                    maximum = squareMatrix[r][c];
                    row = r;
                    column = c;
                }
            }
        }
		
        //Printing out Result with User entered squre Matrix.
	for(int r = 0; r < User_rows; r++){
            for(int c = 0; c < Default; c++){
		if(squareMatrix[r][c] > 1){
                    squareMatrix[r][c] = 1;
		}
		System.out.print(squareMatrix[r][c]);
            }
            System.out.println();
        }

        System.out.print("The largest block is at (" + (row - maximum + 1) + "," + (column - maximum + 1) + ") with "+ maximum + " rows.\n");
    }

}
