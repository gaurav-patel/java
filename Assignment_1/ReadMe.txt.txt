Java444 - Assignment 1
-----------------------
-> Total: 3 Folders, and 10 ".java" files
-> To Run each part need to compile and run all the files
      selected part. Either A, B, or C

Folder-> Part_A:
	--> PhoneBook.java: Purpose is to treat as a phone book to 
			hold multiple numbers from same city but 
			there is limit to number of entries, by 
			default you get 10,000 spaces for phone 
			entry. 

	--> PhoneEntry.java: Purpose is to hold contact number of 
			person with his name, number, and address.
			
	--> Testing.java: File to test the above two files 
			functionality and get desire results. 			

Folder-> Part_B
	--> Book.java: Purpose is to hold all the information on 
		 	books.

	--> CollegeStudent.java: Derived class from Student, holds 
			information about Student's college program,
			semester and classes taken and marks received,
			Purpose is to record student's performance through
			college. 

	--> Course.java: Purpose is to hold all courses offered by college
			and the book object to know which book to buy for
			the course as well.

	--> Gradeable.java: Interface that forces implemented class to
			have specific functionality. Used to convert 
			interger grades to string in Student class.  

	--> Student.java: Holds general information on student like: name,
			studentID, address.

	--> Test.java: Main thread that makes uses of all classes and 
			interface define above to check the functionality
			of all the classes to get desire result.

Folder-> Part_C
	--> Matrix.java: Purpose is to make square Matrix(Box) and get user
			input to fill in boxes. Program finds out the biggest
			block of 1's, and displays where does it start and 
			how big.