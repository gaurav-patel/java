/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Part_A;

/**
 *Testing class tests PhoneBook.java file and PhoneEntry.java file and all it's functions.
 * @author Gaurav
 */
public class Testing {
    public static void main(String[] args){
        //PhoneBook created for Bramptopn for maximum 8 people.
        
        PhoneBook test = new PhoneBook("Brampton",8); 
        //PhoneEntries created for individual people.
        PhoneEntry nirav = new PhoneEntry("Nirav", "205 Queen St", "9052223333");
        PhoneEntry deep = new PhoneEntry("Deep", "110 Pennywood Cres", "9054514422");
        PhoneEntry modi = new PhoneEntry("Patel", "229 Sandlewood Parkway", "6472003000");
        PhoneEntry gaurav = new PhoneEntry("Gaurav", "86 Archdekin Drive", "6471114444");
        PhoneEntry patel = new PhoneEntry("Patel", "89 Dexie Road", "9054445555");
        
        //PhoneEntries are being added to PhoneBook.
        test.add(nirav);
        test.add(deep);
        test.add(modi);
        test.add(gaurav);
        test.add(patel);
        
        //Deleting PhoneEntries by name with 'Patel'.
        PhoneEntry [] test2 = test.deleteByName("Patel");
        
        //Printing new PhoneEntry list.
        for(int i=0; test2[i] != null; i++){
            System.out.print(test2[i].toString() + "\n");
        }
    }
}
