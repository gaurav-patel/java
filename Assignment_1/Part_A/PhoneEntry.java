/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Part_A;

/**
 *Holds information about specific person. Ex: name,number, and address.
 * @author Gaurav
 */
public class PhoneEntry{
    //User define variables used in the class.
    private String name;
    private String address;
    private String phone_number;
    //Default Constructor.
    public PhoneEntry(){ 
        name = null;
        address = null;
        phone_number = null;
    }
    //Over-ride constructor.
    public PhoneEntry(String n, String a, String nm){ //Overload Constructor.
        if(n != null && a != null && nm != null /*>= 1000000000L && n <= 9999999999L*/){
        //Validating data
            name = n;
            address = a;
            phone_number = nm;
        }
    }
    //Converting class variables into string.
    public String toString(){
        return name + " : " + address + " : " + phone_number;
    }
    //Checking if the called object is same with passed in object.
    public boolean equals(PhoneEntry obj){
        if(phone_number == obj.phone_number){
            return true;
        }else{
            return false;
        }
    }
    //Setting/modifying the name of called PhoneEntry.
    public void setName(String n){
        if(n != null){
            name = n;
        }
    }
    //Setting/Modifying the address in the PhoneEntry.
    public void setAddress(String ad){
        if(ad != null){
            address = ad;
        }
    }
    //Setting/Modifying the phone number in the PhoneEntry.
    public void setNumber(String n){
       // if(n >= 1000000000L && n <= 9999999999L){
           phone_number = n;
       // }
    }
    //Getting the name of PhoneEntry.
    public String getName(){
        return name;
    }
    //Getting the address from PhoneEntry.
    public String getAddress(){
        return address;
    }
    //Getting the phone number from PhoneEntry.
    public String getPhoneNumber(){
        return phone_number;
    }
}   
