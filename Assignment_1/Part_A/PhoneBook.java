/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Part_A;

/**
 *PhoneBook class holds PhoneEntry informations from specific city with limit to it. 
 * @author Gaurav
 */
class PhoneBook{
    //User define variables used in the class.
    private String city;
    private int maximum;
    private int entry_used;
    PhoneEntry [] list;
    //Default constructor.
    public PhoneBook(){
    	city = "Markham";
    	maximum = 10000;
    	entry_used = 0;
        list = new PhoneEntry[10000];
    }
    //Over-ride constructor.
    public PhoneBook(String c, int m){
    	if(c != null && m != 0){
            city = c;
            maximum = m;
            list = new PhoneEntry[maximum];
    	}
    }
    //Adds the PhoneEntry to PhoneBook if there is space.
    public boolean add(PhoneEntry entry){
        boolean return_value = false;
        if(entry_used < maximum){
            list[entry_used++] = entry;
            return_value = true;
        }else{
            System.err.print("No more space in PhoneBook.");
        }
        
        return return_value;
    }
    //Returns new PhoneEntry Array after deleting entries with inputed name.
    public PhoneEntry[] deleteByName(String name){
        PhoneEntry [] ret_value = new PhoneEntry[maximum];
        int index = 0;
        
        for(int i = 0; i < entry_used; i++){
            if(!(list[i].getName().equals(name))){
                ret_value[index++] = list[i];
            }
        }
        return ret_value;
    }
    //Converting class variables into string.
    public String toString(){
        String ret_value = "City: " + city + ", Number of Entries: " + entry_used;
        for(int i = 0; i < entry_used; i++){
            ret_value += "\n" + list[i].toString();
        }
        
        return ret_value;
    }
}
