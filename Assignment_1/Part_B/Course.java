/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Part_B;

/**
 *Holds information about what Courses Name, Description, ID, and required book.
 * @author Gaurav
 */
public class Course{
    //User define variables used in this class.
    private String Name;
    private String id;
    private String description;
    private Book Book_name;
    //Default constructor.
    public Course(){
        Name = null;
        id = null;
        description = null;
        Book_name = new Book();
    }
    //Over-ride constructor.
    public Course(String n, String i, String desc, Book b){
       if(n != null && i != null && desc != null && b != null){
           Name = n;
           id = i;
           description = desc;
           Book_name = b;
       }
    }
    //Converting class variables into string.
    public String toString(){
        String ret_value = "Name: " + Name + ", ID: " + id + "\nDescription: " + description;
        ret_value += "\nBook: " + Book_name.toString();
        
        return ret_value;
    }
    //Checking if the called object is same with passed in object.
    public boolean equals(Course obj){
        boolean ret_value = false;
        
        if(!Book_name.equals(obj.Book_name)){
            if((!Name.equals(obj.Name)) && !(id.equals(obj.id)) && !(description.equals(obj.description))){
                ret_value = true;
            }
        }
           
        return ret_value;
    }
    //For making a copy of a object.
    public Course clone(Course obj){
        Course ret_value = new Course(obj.Name, obj.id, obj.description, obj.Book_name);
        
        return ret_value;
    }
}
