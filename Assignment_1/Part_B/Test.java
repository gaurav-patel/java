/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Part_B;

/**
 *Test's  Book.java, CollegeStudent.java, Course.java, Gradeable.java, Student.java and it's functionality.
 * @author Gaurav
 */
public class Test{ //Testing Part_B  
    
    public static void main(String[] args){
        //Making objects to input into classes.
        Book [] books = {
            new Book("College Mathametics Level 1", 127273832, 54.59),
            new Book("A guide to Database using SQL", 232323423, 19.99),
            new Book("Guide to Programming in C++", 234435332, 14.99)
        };

        Course [] classes = {
            new Course("College Math", "MTH101", "College Mathameics", books[0]),
            new Course("SQL Database", "DBS201", "Intro to Database Design with SQL", books[1]),
            new Course("C++ Programming", "OOP345", "Object Oriented Programing using C++", books[2])
        };
        
        CollegeStudent gaurav = new CollegeStudent("Gaurav Patel", 123456789, "Brampton Street", 12, "Brampton", "Ontario", "M5V2E9",
                                    "Computer Programmer", 4, 4);
        
        gaurav.addClass(classes[0]);
        gaurav.addClass(classes[1]);
        gaurav.addClass(classes[2]);
    
        gaurav.submitMarks(classes[0], 65);
        gaurav.submitMarks(classes[1], 75);
        gaurav.submitMarks(classes[2], 85);
        
        //Printing Student's information and calculated marks from corses.
        System.out.println(gaurav.toString());
        System.out.println("Final Grade for Gaurav: " + gaurav.getGrade());
        
        //Making a temp CollegeStudent to run Thread.
        CollegeStudent fake = new CollegeStudent("Fake Name", 8548522, "Toronto Street", 55, "Toronto", "Ontario", "M2S2V6",
                                    "Computer Networking", 2, 5);
        
        Thread t1 = new Thread(fake);
        t1.start();
    }
  
}
