/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Part_B;

/**
 *Interface to convert grade from integer to string.
 * @author Gaurav
 */
public interface Gradeable {
    String grade(int marks);
}
