/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Part_B;

/**
 *Hold information about Book's name, ISBN number, and price.
 * @author Gaurav
 */
public class Book{
    //User define variables used in the class.
    private String Name;
    private long isbn;
    private double price;
    //Default constructor.
    public Book(){
        Name = null;
        isbn = 0;
        price = 0.0;
    }
    //Over-ride constructor.
    public Book(String n, long i, double p){
        if(n != null && i > 0 && p > 0){
            Name = null;
            isbn = i;
            price = p;
        }
    }
    //Converting class variables into string.
    public String toString(){
        return "Name: " + Name + ", ISBN: " + isbn + ", Price: " + price;
    }
    //Checking if the called object is same with passed in object.
    public boolean equals(Book obj){
        boolean ret_value = false;
        
        if(Name == obj.Name && isbn == obj.isbn && price == obj.price){
            ret_value = true;
        }
        
        return ret_value;
    }
    //For making a copy of a object.
    public Book clone(Book obj){
        Book ret_value = new Book(obj.Name, obj.isbn, obj.price);
        
        return ret_value;
    }
}
