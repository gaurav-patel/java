/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Part_B;

/**
 *Holds Student information on Classes, Marks, Program, and Semester.
 * @author Gaurav
 */
public class CollegeStudent extends Student implements Runnable{
    //user define variables used in class.
    private String program;
    private int semester;
    private Course [] classes;
    private int [] marks;
    private int max_size;
    private int index; 
    
    //Default constructor for super class as well.
    public CollegeStudent(){
        super();
        program = null;
        semester = 0;
        classes = new Course[5];
        marks = new int[5];
        max_size = 5;
        index = 0;
    }
    //Over-ride constructor. Calls Super class constructor as well.
    public CollegeStudent(String S_name, long S_id, String S_street, int S_house, String S_town, String S_prov, 
                          String S_zip, String p, int s, int size){
        
        super(S_name, S_id, S_street, S_house, S_town, S_prov, S_zip);
        
        if(p != null && s > 0 && size > 0){
            program = p;
            semester = s;
            max_size = size;
            classes = new Course[size];
            marks = new int[size];
            index = 0;
        }
    }
    //Adding new classe/course.
    public boolean addClass(Course c){
        boolean ret_value = false;
        boolean check = false;
        
        for(int i = 0; i < index; i++){
            if(!classes[i].equals(c)){
                check = true;
            }
        }
        
        if(index < max_size  && check == false){
            classes[index++] = c;
            ret_value = true;
        }else{
            System.err.println("Course can not be added.");
        }
        
        return ret_value;
    }
    //Entering course mark for the selected course.
    public boolean submitMarks(Course c, int grade){
        boolean ret_value = false;
        
        if(grade >= 0 && grade <= 100){
            for(int i = 0; i < index; i++){
                if(!classes[i].equals(c)){
                    marks[i] = grade;
                    i = index;
                    ret_value = true;
                }
            }
        }
        
        return ret_value;
    }
    //Getting Final calculated grades.
    public String getGrade(){
        int final_grade = 0;
        for(int i = 0; i < index; i++){
            final_grade += marks[i]; 
        }
        final_grade = final_grade / index;
        
        //Thread t1 = new Thread(super.grade(final_grade));
        return super.grade(final_grade);
    }
    //Converting class variables into strings.
    public String toString(){
        String ret_value = super.toString() + "\n---------------------------------------\n";
        for(int i = 0; i < index; i++){
            ret_value += classes[i].toString() + "\n---------------------------------------\n";
        }
        
        return ret_value;
    }
   //For running threads.
    @Override
    public void run() {
        System.out.println("Final Marks by thread for fake: " + grade(60));
    }

}