/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Part_B;

/**
 *Holds Student information about Name, StudentID, and Address.
 * @author Gaurav
 */
public class Student implements Gradeable{
    //User define variables used in the class.
    private String Name;
    private long id;
    private Address address;
    //Default constructor.
    public Student(){
        Name = null;
        id = 0;
        address = null;
    }
    //Over-ride constructor.
    public Student(String n, long i, String S_street, int S_house, String S_town, String S_prov, String S_zip){
        if(n != null && i != 0){
            Name = n;
            id = i;
            address = new Address(S_street, S_house, S_town, S_prov, S_zip);
        }
    }
    //Implementing Gradeable, to calculate character grade from interger.
    public String grade(int marks){ //Finds character grade of integer grade.
        String ret_value = "F";
        
        if(marks > 90){
            ret_value = "A+";
        }else if(marks >= 80 && marks <= 89){
            ret_value = "A";
        }else if(marks >= 75 && marks <= 79){
            ret_value = "B+";
        }else if(marks >= 70 && marks <= 74){
            ret_value = "B";
        }else if(marks >= 65 && marks <= 69){
            ret_value = "C+";
        }else if(marks >= 60 && marks <= 64){
            ret_value = "C";
        }else if(marks >= 55 && marks <= 59){
            ret_value = "D+";
        }else if(marks >= 50 && marks <= 54){
            ret_value = "D";
        }
        
        return ret_value;
    }
    //Converting class variables into string.
    public String toString(){
        String ret_value = "Name: " + Name + ", ID: " + id;
        
        return ret_value;
    }
    //Checking if the called object is same with passed in object.
    public boolean equals(Student obj){
        boolean ret_value = false;
        if(Name.equals(obj.Name) && id == obj.id){
            ret_value = true;
        }
        
        return ret_value;
    }
    //For making a copy of a object.
    public Student clone(Student obj){
        Student ret_value = new Student(obj.Name, obj.id, obj.address.street, obj.address.house_number, 
                            obj.address.town, obj.address.province, obj.address.zip_code);
        
        return ret_value;
    }
    
    public class Address{ //Address class is a inner class.
        //User define variables for Address class.
        private String street;
        private int house_number;
        private String town;
        private String province;
        private String zip_code;
        //Default Constructor.
        public Address(){
            street = null;
            house_number = 0;
            town = null;
            province = null;
            zip_code = null;
        }
        //Over-ride constructor.
        public Address(String s, int h, String t, String p, String z){
            if(s != null && h != 0 && t != null && p != null && z != null){
                street = s;
                house_number = h;
                town = t;
                province = p;
                zip_code = z;
            }
        }
        //Converting class variables into string.
        public String toString(){
            String ret_value = "Address: " + house_number + " - "+ street + ", " + town + ", " + province + " " + zip_code;

            return ret_value;
        }
        //Checking if the called object is same with passed in object.
        public boolean equals(Address obj){
            boolean ret_value = false;
            if(street.equals(obj.street) && house_number == obj.house_number && street.equals(obj.street)
                    && town.equals(obj.town) && province.equals(obj.province) && zip_code.equals(obj.zip_code)){
                ret_value = true;
            }

            return ret_value;
        }
        //For making clone of this object.
        public Address clone(Address obj){
            Address ret_value = new Address(obj.street, obj.house_number, obj.town, obj.province, obj.zip_code);

            return ret_value;
        }
    }

}
