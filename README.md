# README #

This repository contains my assignments from college.

Below is overview of what this repository holds but to get in dept description please read ReadMe.txt file in each assignment folder.

### Assignment 1 ###

* Basic java code - uses inheritance and other techniques.
* Part A: a phone book program that holds contact information.
* Part B: application that manages students, courses, and their courses.
* Part C: Purpose is to make square box and allowing user to input 1's into it and identifying the biggest 1's block.

### Assignment 2 ###

* Part A: Shapes classes that holds properties of their shape and few methods.
* Part B: Picture object that holds multiple shapes to make an picture and calculate it's parameter and area, the picture is created from reading a text file.
* Test: The purpose is to test all the classes and make sure they work properly and provide result required by each class function.

### Assignment 3 ###

* Uses Remote Method Invocation (RMI) techniques to send students and timetable objects to server for being processed afterwards it displays processed objects.