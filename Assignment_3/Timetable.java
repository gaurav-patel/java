import java.util.Map;
import java.util.HashMap;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.Serializable;
import java.time.LocalTime;
import java.util.Objects;

/**
 * Timetable object contains Students Monday to Friday's classes schedule.
 * @author Gaurav
 */
public class Timetable implements Serializable {
    private Map<String, String> Monday;
    private Map<String, String> Tuesday;
    private Map<String, String> Wednesday;
    private Map<String, String> Thursday;
    private Map<String, String> Friday;
    
    /**
     * Timetable constructor, creates timetable object using the passed in filename.
     * @param fname receives name of file to read and create timetable object on that file. 
     */
    public Timetable(String fname) {
        Monday = new HashMap();
        Tuesday = new HashMap();
        Wednesday = new HashMap();
        Thursday = new HashMap();
        Friday = new HashMap();
        
        if(fname != null) {
            try {
                BufferedReader inputstream = new BufferedReader(new FileReader(fname));
                String line; 
                
                //For calculating time this is start time.
                int lineNo = 0;
               
                while((line = inputstream.readLine()) != null) {
                    //for adding start time for class to schedule
                    lineNo++;
                    LocalTime time = LocalTime.of(7,05);
                    time = time.plusMinutes((lineNo * 55));
                    
                    //System.out.println(line);
                    String[] data = line.split(",");
                    
                    if(data.length >= 1){
                        if(!data[0].equals("\"\"")) {
                            Monday.put(time.toString(), data[0]);
                        }
                    }
                    
                    if(data.length >= 2){
                        if(!data[1].equals("\"\"")){
                            Tuesday.put(time.toString(), data[1]);
                        }
                    }
                    if(data.length >= 3){
                        if(!data[2].equals("\"\"")) {
                            Wednesday.put(time.toString(), data[2]);
                        }
                    }
                    if(data.length >= 4){
                        if(!data[3].equals("\"\"")) {
                            Thursday.put(time.toString(), data[3]);
                        }
                    }
                    if(data.length == 5){
                        if(!data[4].equals("\"\"")) {
                            Friday.put(time.toString(), data[4]);
                        }
                    }
                }//End of While loop
                
            }catch(FileNotFoundException file) {
                System.out.println("File '" + fname + "' not found!");
            }catch(IOException e) {
                e.printStackTrace();
            }
        }
    } //End of constructor(filename).
    
    /**
     * Checks if specified list by user contains the key.
     * @param map Name of the list to look at.
     * @param key to look for in the list.
     * @return True if specified list contains key, False otherwise.
     */
    public boolean contains(String map, String key) {
        boolean ret_value = false;
        switch(map) {
            case "Monday" :
                ret_value = Monday.containsKey(key);
                break;
            case "Tuesday" :
                ret_value = Tuesday.containsKey(key);
                break;
            case "Wednesday" :
                ret_value = Wednesday.containsKey(key);
                break;
            case "Thursday" :
                ret_value = Thursday.containsKey(key);
                break;
            case "Friday" :
                ret_value = Friday.containsKey(key);
                break;
        }
    
        return ret_value;
    } //End of contains().
    
    /**
     * Gets the value of passed in key in at specified list.
     * @param map The list to take a look at.
     * @param key To find the value of the passed in key.
     * @return the value held at passed in key.
     */
    public String getValue(String map, String key) {
        String ret_value = "";
        
        switch(map) {
            case "Monday" :
                ret_value = Monday.get(key);
                break;
            case "Tuesday" :
                ret_value = Tuesday.get(key);
                break;
            case "Wednesday" :
                ret_value = Wednesday.get(key);
                break;
            case "Thursday" :
                ret_value = Thursday.get(key);
                break;
            case "Friday" :
                ret_value = Friday.get(key);
                break;
        }
        
        return ret_value;
    } //End of getValue().
    
    /**
     * This method finds and returns class held at provided day and period.
     * @param day The day to check for the class.
     * @param period The period to check for student's class.
     * @return The class held at provided day and period, returns "Free" if there is no class.
     */
    public String getCourse(String day, int period) {
        String ret_value = "Free";
        
        LocalTime time = LocalTime.of(7,05);
        time = time.plusMinutes(period * 55);
        
        String key = time.toString();
        
        switch(day) {
            case "Monday" :
                if(Monday.containsKey(key)) {
                    ret_value = Monday.get(key);
                }
                break;
            case "Tuesday" :
                if(Monday.containsKey(key)) {
                    ret_value = Tuesday.get(key);
                }
                break;
            case "Wednesday" :
                if(Monday.containsKey(key)) {
                    ret_value = Wednesday.get(key);
                }
                break;
            case "Thursday" :
                if(Monday.containsKey(key)) {
                    ret_value = Thursday.get(key);
                }
                break;
            case "Friday" :
                if(Monday.containsKey(key)) {
                    ret_value = Friday.get(key);
                }
                break;
        }
        
        return ret_value;
    } //End of getCourse().
    
    /**
     * This method generates String form of the object.
     * @return Object's data as name and values pair as a string.
     */
    @Override
    public String toString() {
        return "Timetable {" + "\n\tMonday = " + Monday + "\n\tTuesday = " + Tuesday 
                            + "\n\tWednesday = " + Wednesday + "\n\tThursday = " + Thursday 
                            + "\n\tFriday = " + Friday 
                + "\n}";
    }
    
    /**
     * This method generates unique object identifier to check against Object method. 
     * @return Unique identifier of specific object.
     */
    @Override
    public int hashCode() {
        int hash = 7;
        hash = 11 * hash + Objects.hashCode(this.Monday);
        hash = 11 * hash + Objects.hashCode(this.Tuesday);
        hash = 11 * hash + Objects.hashCode(this.Wednesday);
        hash = 11 * hash + Objects.hashCode(this.Thursday);
        hash = 11 * hash + Objects.hashCode(this.Friday);
        return hash;
    }
    
    /**
     *This method checks if the current object is equals to passed in object.
     * @param obj Object to compare the current object.
     * @return True if test passes, False otherwise.
     */
    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Timetable other = (Timetable) obj;
        if (!Objects.equals(this.Monday, other.Monday)) {
            return false;
        }
        if (!Objects.equals(this.Tuesday, other.Tuesday)) {
            return false;
        }
        if (!Objects.equals(this.Wednesday, other.Wednesday)) {
            return false;
        }
        if (!Objects.equals(this.Thursday, other.Thursday)) {
            return false;
        }
        if (!Objects.equals(this.Friday, other.Friday)) {
            return false;
        }
        return true;
    } //End of equals(Object).

} //End of Timetable class.

