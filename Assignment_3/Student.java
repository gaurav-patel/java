import java.io.Serializable;
import java.util.Objects;

/**
 * Student object holds information of one student like name, age and registration ID.
 * @author Gaurav
 */
public class Student implements Serializable {
    private String First_name;
    private String Last_name;
    private int age;
    private int registered_id;
    
    /**
     * Default constructor - creates Student object with default values.
     */
    public Student() {
        First_name = "Gaurav";
        Last_name = "Patel";
        age = 19;
        registered_id = 0;
    }
    
    /**
     * Override constructor - creates student object with provided information.
     * @param f_name - To set first name for the Student.
     * @param l_name - To set last name for the Student.
     * @param a - To set age for the Student.
     */
    public Student(String f_name, String l_name, int a) {
        if(f_name != null && l_name != null && a > 0){
            First_name = f_name;
            Last_name = l_name;
            age = a;
            registered_id = 0;
        }
    }
    
    /**
     * to retrieve Student's first name
     * @return Student's First name
     */
    public String getFirstname() {
        return First_name;
    }
    
    /**
     * to retrieve Student's last name
     * @return Student's last name
     */
    public String getLastname() {
        return Last_name;
    }
    
    /**
     * to retrieve Student's age
     * @return Student's age
     */
    public int getAge() {
        return age;
    }
    
    /**
     * to retrieve Student's registration ID
     * @return Student's registration ID
     */
    public int getRegisteredid() {
        return registered_id;
    }
    
    /**
     * To set new name/modify the Student's first name
     * @param f_name new first name to set for the Student. 
     */
    public void setFirstname(String f_name) {
        if(f_name != null)
            First_name = f_name;
    }
    
    /**
     * To set new name/modify the Student's last name
     * @param l_name new last name to set for the Student. 
     */
    public void setLast_name(String l_name) {
        if(l_name != null)
            Last_name = l_name;
    }
    
    /**
     * To set new name/modify the Student's age
     * @param a new age to set for the Student. 
     */
    public void setAge(int a) {
        if(a > 0)
            age = a;
    }
    
    /**
     * To set new name/modify the Student's registration ID
     * @param id new registration ID to set for the Student. 
     */
    public void setRegistered_id(int id) {
        if(id > 0)
            registered_id = id;
    }
    
    
    /**
     * This method generates unique object identifier to check against Object method.  
     * @return hash value for the object.
     */
    @Override
    public int hashCode() {
        int hash = 5;
        hash = 83 * hash + Objects.hashCode(this.First_name);
        hash = 83 * hash + Objects.hashCode(this.Last_name);
        hash = 83 * hash + this.age;
        return hash;
    }
    
    /**
     * This method checks if the current object is equals to passed in object.
     * @param obj Object to compare the current object.
     * @return True if object is same, False otherwise.
     */
    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Student other = (Student) obj;
        if (!Objects.equals(this.First_name, other.First_name)) {
            return false;
        }
        if (!Objects.equals(this.Last_name, other.Last_name)) {
            return false;
        }
        if (this.age != other.age) {
            return false;
        }
        if (this.registered_id != other.registered_id) {
            return false;
        }
        return true;
    } //End of equals(Object).
    
    /**
     * This method generates String form of the object and returns it.
     * @return Object's data as name and values pair in a string.
     */
    @Override
    public String toString() {
        return "First name:" + First_name + ", Last name:" + Last_name + ", age:" + age + ", registered ID:" + registered_id;
    }
    
}
