import java.io.EOFException;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.InetAddress;
import java.net.Socket;
import java.util.Objects;


/**
 * This Client object makes a connection to server object and sends queries and receives
 * objects sent by server.
 * @author Gaurav
 */
public class Client extends Thread {
    private Socket socket;
    private ObjectInputStream input;
    private ObjectOutputStream output;
    private Student student;
    private Timetable schedule;
    private String day;
    private int period;
    
    /**
     * Override constructor - Creates client object using passed in information.
     * @param s Specified socket to use port on client computer. 
     * @param stud student's object to process on the server.
     * @param sch Timetable object to process on the server.
     */
    public Client(Socket s, Student stud, Timetable sch) {
        socket = s;
        student = stud;
        schedule = sch;
        //predefine search day and period to make it simple.
        day = "Monday";
        period = 2;
        
        try {
            input = new ObjectInputStream(socket.getInputStream());
            output = new ObjectOutputStream(socket.getOutputStream());
        }catch(IOException e){
            e.printStackTrace();
        }
    } //End of constructor.
    
    /**
     * Override function of Thread class which is used to send queries to server and 
     * receive the result from server.
     */
    @Override
    public void run(){
        try {
            //sending Student and Timetable object to server
            output.writeObject(student);
            output.flush();
                
            output.writeObject(schedule);
            output.flush();
            
            //printing un-registerd student object between --- lines to seperate object.
            System.out.print("-----------------------------------------\n");
            System.out.print("Student Object before server invocation: \n" + student + "\n");
            System.out.print("-----------------------------------------\n\n");
            
            //reading incomming object from server/socket
            student = (Student) input.readObject();
            
                
            //Client check to see if the Student has class at particular day and time.
            output.writeObject(day);
            output.writeObject(period);
                
            String course = (String) input.readObject();
            course = course.replace("\"", "");
            
            //Printing registerd student object between --- lines to separate object.
            System.out.print("-----------------------------------------\n");
            System.out.print("Student Object after server invocation: \n" + student + "\n");
            System.out.println( student.getFirstname() + "'s schedule on " + day + " at " + period + " period is " + course);
            System.out.print("-----------------------------------------\n\n");
            
        }catch(EOFException eof) {
            System.out.println("The server has terminated connection!");
        }catch(ClassNotFoundException c){
            c.printStackTrace();
        }catch(IOException e) {
            e.printStackTrace();
        } //End of try-catch for processing client's queries.
        
        try {
            //Closing connections
            System.out.println("Client is now closing connections to server.");
            socket.close();
            input.close();
            output.close();
        }catch(IOException e){
            e.printStackTrace();
        }
        
    } //End of run().
    
    /**
     * This method generates unique object identifier to check against Object method. 
     * @return Unique identifier of specific object.
     */
    @Override
    public int hashCode() {
        int hash = 7;
        hash = 67 * hash + Objects.hashCode(this.socket);
        hash = 67 * hash + Objects.hashCode(this.input);
        hash = 67 * hash + Objects.hashCode(this.output);
        hash = 67 * hash + Objects.hashCode(this.student);
        hash = 67 * hash + Objects.hashCode(this.schedule);
        hash = 67 * hash + Objects.hashCode(this.day);
        hash = 67 * hash + this.period;
        return hash;
    }

    /**
     * This method checks if the current object is equals to passed in object.
     * @param obj Object to compare the current object.
     * @return True if test passes, False otherwise.
     */
    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Client other = (Client) obj;
        if (!Objects.equals(this.socket, other.socket)) {
            return false;
        }
        if (!Objects.equals(this.input, other.input)) {
            return false;
        }
        if (!Objects.equals(this.output, other.output)) {
            return false;
        }
        if (!Objects.equals(this.student, other.student)) {
            return false;
        }
        if (!Objects.equals(this.schedule, other.schedule)) {
            return false;
        }
        if (!Objects.equals(this.day, other.day)) {
            return false;
        }
        if (this.period != other.period) {
            return false;
        }
        return true;
    } //End of equals(Object).

    /**
     * This method generates String form of the object.
     * @return Object's data as name and values pair as a string.
     */
    @Override
    public String toString() {
        return "Client {" 
                    + "\n\tsocket = " + socket + ", \n\tinput = " + input + ", \n\toutput = " + output 
                    + ", \n\tstudent = " + student + ", \n\tschedule = " + schedule 
                    + ", \n\tFind class on = " + day + " period " + period 
                + "\n}";
    }
    
    /**
     * Client's main method which creates Timetable and Student objects to process by
     * server. Each student's queries are ran in separate thread and new socket ports
     * are establish each time because one computer cannot use one port for communicating
     * with server about new Student at same time.
     * @param args arguments passes with running this program (not used by program).
     */
    public static void main(String[] args) {
        //Students & timetable object to use for testing server.
        Timetable schedule = new Timetable("timetable.txt");
        
        Student[] s = {new Student("gaurav", "patel", 19), new Student("g", "p", 19), new Student("j", "patel", 20),
                       new Student("m", "patel", 19), new Student("mm", "patel", 19) };
        
        
        //Running client threads.
        int socketnum = 7122;
        
        for(int i=0; i < s.length; i++){
            try {
                /* Creating socket for client to use to send data and get incoming data for 
                    single client, as for loop runs, socket number increases because we are 
                    running all threads on 1 computer so cannot use 1 port for every thread.
                */
                Socket socket = new Socket(InetAddress.getByName("localhost"), socketnum++);
                
                //Creating and running thread on single student.
                new Thread(new Client(socket, s[i], schedule)).start();
                
                
            }catch(IOException e) {
                e.printStackTrace();
            }
        } //End of for loop.
        
    } //End of main loop.
} //End of Client class.
