import java.io.EOFException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.time.LocalTime;
import java.util.Objects;


/**
 * This server class processes clients requests on Student and Timetable objects.
 * @author Gaurav
 */
public class Server extends Thread {
    private Socket connection;
    private ObjectOutputStream output;
    private ObjectInputStream input;
    private Student student;
    private Timetable timetable;
    
    /**
     * Server constructor takes a socket and creates incoming and outgoing connections.
     * @param socket used to create connections between server and client.
     */
    public Server(Socket socket){
        connection = socket;
        
        try {
            output = new ObjectOutputStream(connection.getOutputStream());        
            input = new ObjectInputStream(connection.getInputStream());
        }catch(IOException e){
            e.printStackTrace();
        }
    }
    
    /**
     * Creates a schedule file for the Student using Timetable object. 
     * @param s Student object to know who's schedule file is created.
     * @param t Timetable object contains data to make schedule.
     */
    public static void makefile(Student s, Timetable t){
    //Reads timetable object and creats timetable file for student.
        PrintWriter write = null;
        
        try {
            //create a unique file for specific student.
            write = new PrintWriter(new FileWriter(s.getRegisteredid() + "_timetable.txt"));
            
            //days and time object to save timetable in text file in order. 
            String[] days = new String[] {"Monday", "Tuesday", "Wednesday", "Thursday", "Friday"};
            LocalTime time_track = LocalTime.of(7,05);
            
            //writing one line at a time into file.
            for(int line=1; line <= 12; line++){
                time_track = time_track.plusMinutes(55);
                String fileline = "";
                
                //Making lines from Timetable object.
                for(int row=0; row < 5; row++){
                    if(t.contains(days[row], time_track.toString())){
                        fileline += t.getValue(days[row], time_track.toString());
                    }else{
                        fileline += "\"\"";
                    }
                    
                    if(row != 4){
                        fileline += ",";
                    }
                }
                //writing created line into file.
                write.println(fileline);
            }
            
        }catch(IOException e) {
            e.printStackTrace();
        }finally { //closing PrintWriter stream.
            if(write != null) {
                write.close();
            }
        }
    } //End of makefile().
    
    /**
     * Override function of Thread class which is used to process clients queries and
     * sends results of these queries back in thread.
     */
    @Override
    public void run(){
        System.out.println("Server: " + connection + " is ready for client request..");
        
        try {
                 
            //read Student and Timetable object and creates timetable file for only new student. 
            student = (Student) input.readObject();
            timetable = (Timetable) input.readObject();
                
            //makes a schedule file if it's only new student.
            if(student.getRegisteredid() == 0){
                student.setRegistered_id(student.hashCode());
                makefile(student, timetable);
            }
                
            //sends object to client
            output.writeObject(student);
            output.flush();
                
            //Creating Timetable object from the Student's timetable.txt file for processing search
            //    on student's schedule. Looking to see if student has class on specific time of the day.
            Timetable t = new Timetable(student.getRegisteredid() + "_timetable.txt");
                
            //Checking if student has class at specific day and time.
            String day = (String) input.readObject();
            int period = (int) input.readObject();
                
            String course = t.getCourse(day, period);
                
            //Sending the class at specific day and time.
            output.writeObject(course);
            output.flush();
                
        }catch(EOFException eof) {
            System.out.println("Server: Closing server connections.");
        }catch(ClassNotFoundException c){
            c.printStackTrace();
        }catch(IOException e) {
            e.printStackTrace();
        } //End of try-catch for processing server queries.
        
        try{
            System.out.println("Server: Closing server port " + connection);
            //Closing connections
            connection.close();
            output.close();
            input.close();
        }catch(IOException e){
            e.printStackTrace();
        }
    } //End of void run() function.
    
    /**
     * Server classes main method which runs server and creates sockets for clients to use, only
     * 7122 to 7127 sockets are used because there aren't many clients trying to connect.
     * @param args arguments passes with running this program (not used by program).
     */
    public static void main(String[] args) {
        ServerSocket serversocket;
        int socketnum = 7122;
        int max = 7128;
        boolean run = true;
        
        while(socketnum < max && run){
            try {
                    serversocket = new ServerSocket(socketnum++);
                    Socket connection = serversocket.accept();
                    new Thread(new Server(connection)).start();
                    
                    if(socketnum == max - 1){
                        run = false;
                    }
            }catch(IOException e){
                e.printStackTrace();
            }
        }
        System.out.println("Closing Server.");
    } //End of static main(args). ]

    /**
     * This method generates unique object identifier to check against Object method. 
     * @return Unique identifier of specific object.
     */
    @Override
    public int hashCode() {
        int hash = 3;
        hash = 17 * hash + Objects.hashCode(this.connection);
        hash = 17 * hash + Objects.hashCode(this.output);
        hash = 17 * hash + Objects.hashCode(this.input);
        hash = 17 * hash + Objects.hashCode(this.student);
        hash = 17 * hash + Objects.hashCode(this.timetable);
        return hash;
    }
    
    /**
     * This method checks if the current object is equals to passed in object.
     * @param obj Object to compare the current object.
     * @return True if test passes, False otherwise.
     */
    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Server other = (Server) obj;
        if (!Objects.equals(this.connection, other.connection)) {
            return false;
        }
        if (!Objects.equals(this.output, other.output)) {
            return false;
        }
        if (!Objects.equals(this.input, other.input)) {
            return false;
        }
        if (!Objects.equals(this.student, other.student)) {
            return false;
        }
        if (!Objects.equals(this.timetable, other.timetable)) {
            return false;
        }
        return true;
    } //End of equals(object).

    /**
     * This method generates String form of the object.
     * @return Object's data as name and values pair as a string.
     */
    @Override
    public String toString() {
        return "Server {" 
                + "\n\tconnection = " + connection + ", \n\tstudent = " + student 
                + ", \n\ttimetable = " + timetable
            + "\n}";
    }

} //End of Server class.
