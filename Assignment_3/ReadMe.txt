Java444 - Assignment 3
-----------------------
-> Total: 4 ".java" and 1 ".txt" files

Files:
	--> Server.java: When running program, make sure to run this file first. It creates a Server
		and its socket and waits for clients connection requests after connection it processes
		clients queries in thread. Because of hard coding only 5 sockets are created because 
		client processes only 5 students.

	--> Client.java: After running server, run this file so Client class will create 5 sockets on 
		localhost for running students queries and transfer objects between server and client.
		Each Student is ran on separate thread. Student objects and one timetable object is 
		created from beginning to test for multi-threading server.

	--> Student.java: Holds information on each student like first name, last name, registration ID.
		Used by server and client class to process query on student.

	--> Timetable.java: Holds students schedule for Monday to Friday, Timetable object created from 
		a text file. Used by server and client class to find out which class student has on the
		day and period (hard coded so chose Monday and 2nd period from start).

	--> timetable.txt: A timetable file that contain schedule for timetable object. This file is used 
		all the students timetable for testing.

